package ru.mynewtons.yml.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.List;

@Data
@XStreamAlias("shop")
public class Shop {

    private List<Currency> currencies;

    private List<Category> categories;

    private List<Offer> offers;

}
