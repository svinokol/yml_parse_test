package ru.mynewtons.yml.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Data
@XStreamAlias("offer")
public class Offer {

    @JacksonXmlProperty(isAttribute = true)
    @XStreamAsAttribute
    private String id;

    private String name;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty( localName = "picture" )
    @XStreamImplicit(itemFieldName = "picture")
    private List<String> pictures;

    private String description;

    @JacksonXmlProperty(isAttribute = true)
    @XStreamAsAttribute
    private Boolean available;

    @JacksonXmlProperty(isAttribute = true, localName = "group_id")
    @XStreamAlias("group_id")
    @XStreamAsAttribute
    private String groupId;

    private String price;

    private String vendor;

    private String vendorCode;

    private String currencyId;

    private String categoryId;

    private Integer stock;

    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "param")
    @XStreamImplicit
    protected List<Param> params;
}
