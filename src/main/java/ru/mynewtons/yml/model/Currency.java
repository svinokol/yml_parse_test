package ru.mynewtons.yml.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import lombok.Data;

@Data
@XStreamAlias("currency")
public class Currency {

    @JacksonXmlProperty(isAttribute = true)
    @XStreamAsAttribute
    private String id;

}
