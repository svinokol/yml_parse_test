package ru.mynewtons.yml.config;

import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.xml.StaxEventItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.oxm.xstream.XStreamMarshaller;
import ru.mynewtons.yml.mapper.ModelMapper;
import ru.mynewtons.yml.model.*;
import ru.mynewtons.yml.model.Currency;
import ru.mynewtons.yml.service.OfferService;
import ru.mynewtons.yml.service.ProductService;

import java.util.*;
import java.util.stream.Collectors;


@Configuration
@Slf4j
@EnableBatchProcessing
public class OfferBatchConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private OfferService offerService;

    @Autowired
    private ModelMapper modelMapper;



    @Bean
    public StaxEventItemReader<Catalog> reader() {
        StaxEventItemReader<Catalog> reader = new StaxEventItemReader<>();
        reader.setResource(new ClassPathResource("/xml/yml_test_new.xml"));
        reader.setFragmentRootElementName("yml_catalog");
        XStreamMarshaller xStreamMarshaller = new XStreamMarshaller();
        xStreamMarshaller.setAutodetectAnnotations(true);
        xStreamMarshaller.setAnnotatedClasses(Catalog.class, Currency.class, Param.class, Category.class, Offer.class);
        xStreamMarshaller.setNameCoder(new XmlFriendlyNameCoder("_-", "_"));
        reader.setUnmarshaller(xStreamMarshaller);
        return reader;
    }

    @Bean
    public ItemProcessor<Catalog, List<ru.mynewtons.yml.domain.Offer>> processor() {
        return catalog ->
            catalog.getShop().getOffers().stream()
                    .map(offer -> modelMapper.map(offer, ru.mynewtons.yml.domain.Offer.class))
                    .collect(Collectors.toList());
    }

    @Bean
    public ItemWriter<List<ru.mynewtons.yml.domain.Offer>> writer() {
        return new ItemWriter<List<ru.mynewtons.yml.domain.Offer>>() {
            @Override
            public void write(List<? extends List<ru.mynewtons.yml.domain.Offer>> items) throws Exception {
                items.forEach(o -> {
                    o.forEach(offer -> {
                        log.info("Update offer "+ offer.getName());
                        offerService.save(offer);
                    });
                });
           }
       };
    }



    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1")
                .<Catalog, List<ru.mynewtons.yml.domain.Offer>>chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }

    @Bean
    public Job importOfferJob() {
        return jobBuilderFactory.get("importOffersJob")
                .start(step1())
                .build();
    }

}
