package ru.mynewtons.yml.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "offers")
@Setter
@Getter
public class Offer {

    @Id
    private String id;

    @Column
    private String name;

    @ElementCollection
    private List<String> pictures;

    @Column(columnDefinition = "TEXT")
    private String description;

    @Column
    private String vendor;

    @Column(name = "vendor_code")
    private String vendorCode;

    @Column
    private Boolean available;

    @Column
    private Integer stock;

    @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.MERGE})
    @JoinColumn(name = "product_id")
    @JsonIgnore
    private Product product;

    @Column
    private String price;

    @Column(name = "currency_id")
    private String currencyId;

    @Column(name = "category_id")
    private String categoryId;

    @ElementCollection
    protected Map<String,String> params;

}
